// SPDX-License-Identifier: GPL-2.0

/*
 * Copyright (C) 2024 Alibaba Corporation
 * Author: Shuai Xue
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Constants
#define DWORD_LEN 4
#define MAX_DATA_LEN (1024 * DWORD_LEN)
// MaxTLPBuffer is a 4 dword header + max data payload.
#define MAX_TLP_BUFFER (4 * DWORD_LEN + MAX_DATA_LEN * DWORD_LEN)

// TLP Formats
enum TlpFormat {
	FMT_3DW_NO_DATA = 0b000,
	FMT_4DW_NO_DATA = 0b001,
	FMT_3DW_WITH_DATA = 0b010,
	FMT_4DW_WITH_DATA = 0b011,
	FMT_TLP_PREFIX = 0b100,
};

// TLP Types
typedef uint8_t TlpType;
enum TlpType {
	MRD3 = (FMT_3DW_NO_DATA << 5) |
	       0b00000, // Memory Read Request, 3 dwords
	MRD4 = (FMT_4DW_NO_DATA << 5) |
	       0b00000, // Memory Read Request, 4 dwords
	MRD_LK3 = (FMT_3DW_NO_DATA << 5) |
		  0b00001, // Memory Read Request-Locked, 3 dwords
	MRD_LK4 = (FMT_4DW_NO_DATA << 5) |
		  0b00001, // Memory Read Request-Locked, 4 dwords
	MWR3 = (FMT_3DW_WITH_DATA << 5) |
	       0b00000, // Memory Write Request, 3 dwords
	MWR4 = (FMT_4DW_WITH_DATA << 5) |
	       0b00000, // Memory Write Request, 4 dwords
	IORDT = (FMT_3DW_NO_DATA << 5) | 0b00010, // I/O Read Request
	IOWRTT = (FMT_3DW_WITH_DATA << 5) | 0b00010, // I/O Write Request
	CFGRD0 = (FMT_3DW_NO_DATA << 5) | 0b00100, // Configuration Read Type 0
	CFGWR0 = (FMT_3DW_WITH_DATA << 5) |
		 0b00100, // Configuration Write Type 0
	CFGRD1 = (FMT_3DW_NO_DATA << 5) | 0b00101, // Configuration Read Type 1
	CFGWR1 = (FMT_3DW_WITH_DATA << 5) |
		 0b00101, // Configuration Write Type 1
	CPLE = (FMT_3DW_NO_DATA << 5) | 0b01010, // Completion without Data
	CPLD = (FMT_3DW_WITH_DATA << 5) | 0b01010, // Completion with Data
	CPLLK = (FMT_3DW_NO_DATA << 5) |
		0b01011, // Completion for Locked Read without Data
	CPLLKD = (FMT_3DW_WITH_DATA << 5) |
		 0b01011, // Completion for Locked Read with Data
	MR_IOV = (FMT_TLP_PREFIX << 5) | 0b00000, // MR-IOV TLP Prefix
	LOCAL_VEND_PREFIX = (FMT_TLP_PREFIX << 5) |
			    0b01110, // Local TLP Prefix with vendor sub-field
	EXT_TPH = (FMT_TLP_PREFIX << 5) | 0b10000, // Extended TPH TLP Prefix
	PASID = (FMT_TLP_PREFIX << 5) | 0b10001, // PASID TLP Prefix
	END_END_VEND_PREFIX =
		(FMT_TLP_PREFIX << 5) |
		0b11110 // End-to-End TLP Prefix with vendor sub-field
};

// AddressType is the address type field in the request header.
typedef uint8_t AddressType;
enum AddressType {
	DEFAULT_UNTRANSLATED = 0b00,
	TRANSLATION_REQUEST = 0b01,
	TRANSLATED = 0b10,
	ADDRESS_TYPE_RESERVED = 0b11
};

// TrafficClass is the traffic class field in the request header and used
// to set quality of service (QoS).
typedef uint8_t TrafficClass;
enum TrafficClass { TC0, TC1, TC2, TC3, TC4, TC5, TC6, TC7 };

// CompletionStatus is the completion status field in the completion header.
typedef uint8_t CompletionStatus;
enum CompletionStatus {
	SUCCESSFUL_COMPLETION = 0b000,
	UNSUPPORTED_REQUEST = 0b001,
	CONFIGURATION_REQUEST_RETRY = 0b010,
	COMPLETER_ABORT = 0b100
};

// DataLength decodes length to data length
int data_length(int length)
{
	if (length == 0) {
		return 1024 * DWORD_LEN;
	}
	return length * DWORD_LEN;
}

// TlpHeader is the first header dword, common on all TLPs.
// See section 2.2.1. Common Packet Header Fields.
typedef struct {
	uint8_t type; // Format and Type (8 bits, directly use enum TlpType)

	struct {
		uint8_t th : 1; // TLP Processing Hints (1 bit)
		uint8_t rsv1 : 1; // Reserved bits to fill the byte (1 bits)
		uint8_t a2 : 1;
		uint8_t t8 : 1;
		uint8_t tc : 3; // Traffic class (3 bits)
		uint8_t t9 : 1;
	};

	struct {
		uint16_t len : 10; // Length of data payload
		uint8_t at : 2; // Address Type (2 bits)
		uint8_t attr : 2; // No-Snoop (1 bit)
		uint8_t ep : 1; // Poisoned TLP indicator (1 bit)
		uint8_t td : 1; // TLP Digest presence (1 bit)
	};
} TlpHeader;

// Function to get the string representation of the TlpType
const char *get_tlp_type_string(enum TlpType type)
{
	switch (type) {
	case MRD3:
		return "MRD3 (Memory Read Request, 3 dwords)";
	case MRD4:
		return "MRD4 (Memory Read Request, 4 dwords)";
	case MRD_LK3:
		return "MRD_LK3 (Memory Read Request-Locked, 3 dwords)";
	case MRD_LK4:
		return "MRD_LK4 (Memory Read Request-Locked, 4 dwords)";
	case MWR3:
		return "MWR3 (Memory Write Request, 3 dwords)";
	case MWR4:
		return "MWR4 (Memory Write Request, 4 dwords)";
	case IORDT:
		return "IORDT (I/O Read Request)";
	case IOWRTT:
		return "IOWRTT (I/O Write Request)";
	case CFGRD0:
		return "CFGRD0 (Configuration Read Type 0)";
	case CFGWR0:
		return "CFGWR0 (Configuration Write Type 0)";
	case CFGRD1:
		return "CFGRD1 (Configuration Read Type 1)";
	case CFGWR1:
		return "CFGWR1 (Configuration Write Type 1)";
	case CPLE:
		return "CPLE (Completion without Data)";
	case CPLD:
		return "CPLD (Completion with Data)";
	case CPLLK:
		return "CPLLK (Completion for Locked Read without Data)";
	case CPLLKD:
		return "CPLLKD (Completion for Locked Read with Data)";
	case MR_IOV:
		return "MR_IOV (MR-IOV TLP Prefix)";
	case LOCAL_VEND_PREFIX:
		return "LOCAL_VEND_PREFIX (Local TLP Prefix with vendor sub-field)";
	case EXT_TPH:
		return "EXT_TPH (Extended TPH TLP Prefix)";
	case PASID:
		return "PASID (PASID TLP Prefix)";
	case END_END_VEND_PREFIX:
		return "END_END_VEND_PREFIX (End-to-End TLP Prefix with vendor sub-field)";
	default:
		return "Unknown TLP Type";
	}
}

// Function to dump the TlpHeader structure
void dumpTlpHeader(const TlpHeader *header)
{
	printf("Common TLP Header:\n");
	printf("---------------------------\n");
	printf("\tType\t\t: 0x%x (%s)\n", header->type,
	       get_tlp_type_string(header->type));
	printf("\tTH\t\t: 0x%x\n", header->th);
	printf("\tRSV1\t\t: 0x%x\n", header->rsv1);
	printf("\tA2\t\t: 0x%x\n", header->a2);
	printf("\tT8\t\t: 0x%x\n", header->t8);
	printf("\tTraffic Class\t: 0x%x\n", header->tc);
	printf("\tT9\t\t: 0x%x\n", header->t9);
	printf("\tLength\t\t: 0x%x\n", header->len);
	printf("\tAddress Type\t: 0x%x\n", header->at);
	printf("\tAttributes\t: 0x%x\n", header->attr);
	printf("\tEP\t\t: 0x%x\n", header->ep);
	printf("\tTD\t\t: 0x%x\n", header->td);
}

// DeviceID is a configuration space address that uniquely identifies
// the device on the PCIe fabric.
typedef struct {
	uint8_t bus;
	uint8_t func : 3;
	uint8_t dev : 5;
} DeviceID;

// Function to dump the DeviceID structure
void dumpDeviceID(const char *name, const DeviceID *id)
{
	printf("\t%s\t\t: %04x (%02x:%02x.%x)\n", name, *((uint16_t *)id),
	       id->bus, id->dev, id->func);
}

// RequestHeader extends TlpHeader and includes the second header dword
// on Memory, IO, and Config Request TLPs.
typedef struct {
	TlpHeader tlpHeader; // Embedded TlpHeader struct
	DeviceID reqID; // Requester ID
	uint8_t tag; // Unique tag for outstanding requests
	struct {
		uint8_t firstBE : 4; // First Byte Enable (4 bits)
		uint8_t lastBE : 4; // Last Byte Enable (4 bits)
	};
} RequestHeader;

// CplHeader structure definition
typedef struct {
	TlpHeader tlpHeader; // Embedded TlpHeader

	// Second DWORD
	DeviceID cplID; // Completer ID

	struct {
		uint8_t bc_h : 4; // Byte count (4 bits in Byte 6)
		uint8_t bcm : 1; // Completion status (3 bits)
		uint8_t status : 3; // Completion status (3 bits)
		uint8_t bc_l : 4; // Byte count (8 bits in Byte 7)
	};

	// Third DWORD
	DeviceID reqID; // Requester ID
	struct {
		uint8_t tag; // Unique tag for all outstanding requests
		uint8_t rsv : 1; // Reserved (1 bit to fill the byte)
		uint8_t addressLow : 7; // Lower Byte Address (7 bits)
	};
} CplHeader;

// MRd structure representing Memory Read TLP
typedef struct {
	RequestHeader reqHeader; // Embedded RequestHeader struct
	uint32_t addr; // Address field
} MRd;

// MWr structure representing Memory Write TLP
typedef struct {
	RequestHeader reqHeader; // Embedded RequestHeader struct
	uint32_t addr; // Address field
	uint32_t data[];
} MWr;

// Cpl TLP: Completion response.
typedef struct {
	CplHeader cplHeader;
	uint32_t data[];
} Cpl;

// CfgHeader extends RequestHeader and includes the third header dword
// for configuration read TLPs.
typedef struct {
	RequestHeader reqHeader;
	DeviceID desdID;
	uint8_t extRegNum : 4;
	uint8_t rsv1 : 4;
	uint8_t rsv2 : 2;
	uint8_t regNr : 6;
} CfgHeader;

// CfgRd TLP: Configuration read request.
typedef struct {
	CfgHeader cfgHheader;
} CfgRd;

// IORd TLP: I/O read request.
typedef struct {
	RequestHeader reqHeader;
	uint32_t addr;
} IORd;

// IOWrt TLP: Memory write request.
typedef struct {
	RequestHeader reqHeader;
	uint32_t addr;
	uint32_t data[];
} IOWrt;

void toBDF(uint16_t val)
{
	int bus = val >> 8;
	int dev = (val >> 3) & 0x1f;
	int func = (val & 0x7);
	printf("%02x:%02x.%x\n", bus, dev, func);
}

// Function to reverse byte order of a 32-bit integer
uint32_t reverse_bytes(uint32_t value)
{
	return ((value >> 24) & 0xFF) | ((value >> 8) & 0xFF00) |
	       ((value << 8) & 0xFF0000) | ((value << 24) & 0xFF000000);
}

// Function to convert a hexadecimal string to a 32-bit integer
uint32_t hex_string_to_uint32(const char *hex_str)
{
	return reverse_bytes((uint32_t)strtoul(hex_str, NULL, 16));
}

// Function to convert a string of hexadecimal values to a buffer
void convert_string_to_buffer(const char *input, uint32_t *output, size_t size)
{
	char buffer[9]; // 8 hex digits + null terminator
	size_t i = 0;

	while (*input && i < size) {
		// Read the next 8 hex digits
		strncpy(buffer, input, 8);
		buffer[8] = '\0';
		output[i] = hex_string_to_uint32(buffer);
		input += 9;
		i++;
	}
}

// Function to print the TLP in hexadecimal format
void print_hex(uint32_t *header, size_t size)
{
	printf("\t");
	for (size_t i = 0; i < size; i++) {
		printf("0x%08x ", header[i]);
	}
	printf("\n");
}

// Function to dump the RequestHeader structure
void dumpReqHeader(const RequestHeader *header)
{
	dumpTlpHeader(&header->tlpHeader);

	// Print fields from the second DWORD
	printf("\nSecond DWORD:\n");
	printf("---------------------------\n");
	dumpDeviceID("reqID", &header->reqID);
	printf("\tTag\t\t: 0x%x\n", header->tag);
	printf("\tFirst BE\t: 0x%x\n", header->firstBE);
	printf("\tLast BE\t\t: 0x%x\n", header->lastBE);
}

// Function to dump the CplHeader structure
void dumpCplHeader(const CplHeader *header)
{
	dumpTlpHeader(&header->tlpHeader);

	// Print fields from the second DWORD
	printf("\nSecond DWORD:\n");
	printf("---------------------------\n");
	dumpDeviceID("cplID", &header->cplID);
	printf("\tStatus\t\t: 0x%x\n", header->status);
	printf("\tBCM\t\t: 0x%x\n", header->bcm);
	printf("\tByte Count\t: 0x%x\n", header->bc_l | (header->bc_h << 8));

	// Print fields from the third DWORD
	printf("\nThird DWORD:\n");
	printf("---------------------------\n");
	dumpDeviceID("reqID", &header->reqID);
	printf("\tTag\t\t: 0x%x\n", header->tag);
	printf("\tAddress Low\t: 0x%x\n", header->addressLow);
}

static void dumpCfgHeader(uint32_t *tlp)
{
	CfgHeader *header = (CfgHeader *)tlp;
	dumpReqHeader((RequestHeader *)tlp);
	// Print fields from the second DWORD
	printf("\nSecond DWORD:\n");
	printf("---------------------------\n");
	dumpDeviceID("destID", &header->desdID);
	printf("\tExt Reg Num\t: 0x%x\n", header->extRegNum);
	printf("\tRegister Number\t: 0x%x\n", header->regNr);
}

static void handle_tlp(uint32_t *tlp)
{
	TlpHeader *hdr = (TlpHeader *)tlp;
	switch (hdr->type) {
	case MRD3:
	case MRD_LK3:
	case MWR3:
	case IORDT:
	case IOWRTT:
		dumpReqHeader((RequestHeader *)tlp);
		printf("\nThird DWORD:\n");
		printf("---------------------------\n");
		printf("\tAddress \t: 0x%08x\n", tlp[2]);
		break;
	case MRD4:
	case MRD_LK4:
	case MWR4:
		dumpReqHeader((RequestHeader *)tlp);
		printf("\nThird & Fourth DWORD:\n");
		printf("---------------------------\n");
		printf("\tAddress \t: 0x%08x 0x%08x\n", tlp[2], tlp[3]);
		break;
	case CFGRD0:
	case CFGWR0:
	case CFGRD1:
	case CFGWR1:
		dumpCfgHeader(tlp);
		break;
	case CPLE:
	case CPLD:
	case CPLLK:
	case CPLLKD:
		dumpCplHeader((CplHeader *)tlp);
		break;
	default:
		printf("Unknown TLP Type");
	}
}

// Main function for testing
int main(int argc, char *argv[])
{
	uint32_t buffer[4];

	if (argc != 2) {
		fprintf(stderr, "Usage: %s \"hex_string\"\n", argv[0]);
		fprintf(stderr,
			"./tlp_hdr_decoder \"4a008001 7e010004 7d000030 40000000\"\n");
		return 1;
	}

	printf("Original TLP register:\n");
	printf("\t%s\n", argv[1]);

	// Convert the input string to buffer (little-endian format)
	convert_string_to_buffer(argv[1], buffer, 4);

	// Print the original TLP
	printf("TLP (little-endian):\n");
	print_hex(buffer, 4);
	printf("\n");

	handle_tlp(buffer);

	return 0;
}
