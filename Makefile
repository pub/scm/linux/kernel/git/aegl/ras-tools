# SPDX-License-Identifier: GPL-2.0

CFLAGS	= -O

all: mca-recover vtop cmcistorm hornet einj_mem_uc lmce rep_ce_page memattr victim einj_pcie_err \
	tlp_hdr_decoder

clean:
	rm -f *.o mca-recover vtop cmcistorm hornet einj_mem_uc lmce rep_ce_page memattr victim einj_pcie_err tlp_hdr_decoder

mca-recover: mca-recover.o proc_pagemap.o
	cc -o mca-recover $(CFLAGS) mca-recover.o proc_pagemap.o

vtop: vtop.c
	cc -o vtop $(CFLAGS) vtop.c

cmcistorm: cmcistorm.o proc_pagemap.o einj.o
	cc -o cmcistorm $(CFLAGS) cmcistorm.o proc_pagemap.o einj.o

rep_ce_page: rep_ce_page.o proc_pagemap.o einj.o
	cc -o rep_ce_page $(CFLAGS) rep_ce_page.o proc_pagemap.o einj.o

hornet: hornet.o einj.o
	cc -o hornet $(CFLAGS) hornet.o einj.o

einj_mem_uc: einj_mem_uc.o proc_cpuinfo.o proc_interrupt.o proc_pagemap.o do_memcpy.o einj.o
	cc -o einj_mem_uc einj_mem_uc.o proc_cpuinfo.o proc_interrupt.o proc_pagemap.o do_memcpy.o einj.o -pthread

lmce: proc_pagemap.o lmce.o
	cc -o lmce proc_pagemap.o lmce.o -pthread

memattr: memattr.o einj.o
	cc -o memattr $(CFLAGS) memattr.o einj.o

victim: victim.c
	cc -o victim $(CFLAGS) victim.c

einj_pcie_err: einj_pcie_err.o einj.o
	cc -o einj_pcie_err einj_pcie_err.o einj.o
